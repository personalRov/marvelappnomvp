package com.android.es.roversanz.marvelapp.data.services.transformers

import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.services.contracts.ItemResponse
import com.android.es.roversanz.marvelapp.data.services.contracts.ItemResponseList
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelCharacterOutputContract
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelData
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelResponse
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelThumbnail
import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class CharacterTransformerTest {

    private lateinit var transformer: CharacterTransformer

    @Before
    fun setUp() {
        transformer = CharacterTransformer()
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testTransformer_empty() {
        val outputContract = MarvelResponse(
                status = "status",
                attributionText = "attributionText",
                data = MarvelData<MarvelCharacterOutputContract>(offset = 2, count = 4, results = emptyList())
        )

        PowerMock.replayAll()
        val list = transformer.transformer(outputContract)

        Truth.assertThat(list.size).isEqualTo(0)
    }

    @Test
    fun testTransformer_notEmpty_ItemsEmpty() {
        val results = arrayListOf<MarvelCharacterOutputContract>()
        results.add(MarvelCharacterOutputContract(
                id = 1,
                name = "name",
                description = "description",
                resourceURI = "resourceURI",
                modified = null,
                thumbnail = MarvelThumbnail(path = "path", extension = "extension"),
                comics = ItemResponseList(available = 1, returned = 1, collectionURI = "collectionURI1", items = emptyList()),
                series = ItemResponseList(available = 2, returned = 2, collectionURI = "collectionURI2", items = emptyList()),
                stories = ItemResponseList(available = 3, returned = 3, collectionURI = "collectionURI3", items = emptyList()),
                events = ItemResponseList(available = 4, returned = 4, collectionURI = "collectionURI4", items = emptyList())
        ))
        val outputContract = MarvelResponse(
                status = "status",
                attributionText = "attributionText",
                data = MarvelData(offset = 2, count = 4, results = results)
        )

        PowerMock.replayAll()
        val list = transformer.transformer(outputContract)

        Truth.assertThat(list.size).isEqualTo(1)
        val character: Character = list[0]
        Truth.assertThat(character.id).isEqualTo(1)
        Truth.assertThat(character.name).isEqualTo("name")
        Truth.assertThat(character.description).isEqualTo("description")
        Truth.assertThat(character.resourceURI).isEqualTo("resourceURI")
        Truth.assertThat(character.modifiedDate).isNull()
        Truth.assertThat(character.thumbnail).isEqualTo("path/standard_medium.extension")
        Truth.assertThat(character.picture).isEqualTo("path/landscape_xlarge.extension")
        Truth.assertThat(character.comicsSummary).isNotNull()
        Truth.assertThat(character.comicsSummary.available).isEqualTo(1)
        Truth.assertThat(character.comicsSummary.returned).isEqualTo(1)
        Truth.assertThat(character.comicsSummary.collectionURI).isEqualTo("collectionURI1")
        Truth.assertThat(character.comicsSummary.items).isNotNull()
        Truth.assertThat(character.comicsSummary.items.size).isEqualTo(0)
        Truth.assertThat(character.seriesSummary).isNotNull()
        Truth.assertThat(character.seriesSummary.available).isEqualTo(2)
        Truth.assertThat(character.seriesSummary.returned).isEqualTo(2)
        Truth.assertThat(character.seriesSummary.collectionURI).isEqualTo("collectionURI2")
        Truth.assertThat(character.seriesSummary.items).isNotNull()
        Truth.assertThat(character.seriesSummary.items.size).isEqualTo(0)
        Truth.assertThat(character.storiesSummary).isNotNull()
        Truth.assertThat(character.storiesSummary.available).isEqualTo(3)
        Truth.assertThat(character.storiesSummary.returned).isEqualTo(3)
        Truth.assertThat(character.storiesSummary.collectionURI).isEqualTo("collectionURI3")
        Truth.assertThat(character.storiesSummary.items).isNotNull()
        Truth.assertThat(character.storiesSummary.items.size).isEqualTo(0)
        Truth.assertThat(character.eventsSummary).isNotNull()
        Truth.assertThat(character.eventsSummary.available).isEqualTo(4)
        Truth.assertThat(character.eventsSummary.returned).isEqualTo(4)
        Truth.assertThat(character.eventsSummary.collectionURI).isEqualTo("collectionURI4")
        Truth.assertThat(character.eventsSummary.items).isNotNull()
        Truth.assertThat(character.eventsSummary.items.size).isEqualTo(0)
    }

    @Test
    fun testTransformer_notEmpty_ItemsNotEmpty() {
        val comics = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name1", resourceURI = "resourceURI1")) }
        val series = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name2", resourceURI = "resourceURI2")) }
        val stories = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name3", resourceURI = "resourceURI3")) }
        val events = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name4", resourceURI = "resourceURI4")) }
        val results = arrayListOf<MarvelCharacterOutputContract>()
        results.add(MarvelCharacterOutputContract(
                id = 1,
                name = "name",
                description = "description",
                resourceURI = "resourceURI",
                modified = null,
                thumbnail = MarvelThumbnail(path = "path", extension = "extension"),
                comics = ItemResponseList(available = 1, returned = 1, collectionURI = "collectionURI1", items = comics),
                series = ItemResponseList(available = 2, returned = 2, collectionURI = "collectionURI2", items = series),
                stories = ItemResponseList(available = 3, returned = 3, collectionURI = "collectionURI3", items = stories),
                events = ItemResponseList(available = 4, returned = 4, collectionURI = "collectionURI4", items = events)
        ))
        val outputContract = MarvelResponse(
                status = "status",
                attributionText = "attributionText",
                data = MarvelData(offset = 2, count = 4, results = results)
        )

        PowerMock.replayAll()
        val list = transformer.transformer(outputContract)

        val character: Character = list[0]
        Truth.assertThat(character.comicsSummary.items).isNotNull()
        val comic = character.comicsSummary.items[0]
        Truth.assertThat(comic.name).isEqualTo("name1")
        Truth.assertThat(comic.resourceURI).isEqualTo("resourceURI1")
        Truth.assertThat(character.seriesSummary.items).isNotNull()
        val serie = character.seriesSummary.items[0]
        Truth.assertThat(serie.name).isEqualTo("name2")
        Truth.assertThat(serie.resourceURI).isEqualTo("resourceURI2")
        Truth.assertThat(character.storiesSummary.items).isNotNull()
        val story = character.storiesSummary.items[0]
        Truth.assertThat(story.name).isEqualTo("name3")
        Truth.assertThat(story.resourceURI).isEqualTo("resourceURI3")
        Truth.assertThat(character.eventsSummary.items).isNotNull()
        val event = character.eventsSummary.items[0]
        Truth.assertThat(event.name).isEqualTo("name4")
        Truth.assertThat(event.resourceURI).isEqualTo("resourceURI4")
    }

}

package com.android.es.roversanz.marvelapp.ui

import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.data.services.ComicListener
import com.android.es.roversanz.marvelapp.data.services.MarvelRepository
import org.easymock.EasyMock
import org.easymock.EasyMock.eq
import org.easymock.EasyMock.isA
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(Comic::class, Character::class, ComicListener::class)
class MainActivityPresenterImplTest {

    lateinit var presenter: MainActivityContract.MainActivityPresenter

    lateinit var marvelRepository: MarvelRepository
    lateinit var view: MainActivityContract.MainActivityView

    @Before
    fun setUp() {
        view = PowerMock.createMock(MainActivityContract.MainActivityView::class.java)
        marvelRepository = PowerMock.createMock(MarvelRepository::class.java)
        presenter = MainActivityPresenterImpl(marvelRepository)
        presenter.attach(view)
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testPrepareViewComics() {

        EasyMock.expect(marvelRepository.getComics(isA(ComicListener::class.java), eq(0)))

        view.showLoading()

        PowerMock.replayAll()
        presenter.prepareView()
    }

    @Test
    fun testRefreshItems_ZeroComics() {
        EasyMock.expect(marvelRepository.getComics(isA(ComicListener::class.java), eq(0)))

        view.showLoading()

        PowerMock.replayAll()
        presenter.refreshItems(0)
    }

    @Test
    fun testRefreshItems_notZeroComics() {
        EasyMock.expect(marvelRepository.getComics(isA(ComicListener::class.java), eq(20)))

        view.showLoading()

        PowerMock.replayAll()
        presenter.refreshItems(20)
    }

    @Test
    fun testOnCharacterSelected() {
        val character = PowerMock.createMock(Character::class.java)

        view.showCharacterDetail(character)

        PowerMock.replayAll()
        presenter.onCharacterSelected(character)
    }

    @Test
    fun testOnComicSelected() {
        val comic = PowerMock.createMock(Comic::class.java)

        view.showComicDetail(comic)

        PowerMock.replayAll()
        presenter.onComicSelected(comic)
    }


}

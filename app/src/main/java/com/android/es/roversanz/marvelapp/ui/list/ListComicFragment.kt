package com.android.es.roversanz.marvelapp.ui.list

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.android.es.roversanz.marvelapp.ApplicationProject
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.di.components.MainComponent
import com.android.es.roversanz.marvelapp.di.scopes.ActivityScope
import com.android.es.roversanz.marvelapp.di.scopes.FragmentScope
import com.android.es.roversanz.marvelapp.ui.base.BaseFragment
import com.android.es.roversanz.marvelapp.utils.EndlessRecyclerViewScrollListener
import com.android.es.roversanz.marvelapp.utils.adapters.CharactersAdapter
import com.android.es.roversanz.marvelapp.utils.adapters.ComicsAdapter
import com.android.es.roversanz.marvelapp.utils.adapters.ComicsAdapterListener
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.fragment_comic_list.comics_empty
import kotlinx.android.synthetic.main.fragment_comic_list.comics_rv
import kotlinx.android.synthetic.main.fragment_comic_list.comics_swipe
import org.parceler.Parcels

class ListComicFragment : BaseFragment<ListComicFragmentContract.ListComicFragmentView, ListComicFragmentContract.ListComicFragmentPresenter, ListComicFragment.Callback>(),
        SwipeRefreshLayout.OnRefreshListener,
        ListComicFragmentContract.ListComicFragmentView,
        ComicsAdapterListener {

    companion object {
        private val KEY_COMICS = "key:comic_list"

        fun newBundle(items: List<Comic>) = ListComicFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_COMICS, Parcels.wrap(items))
            }
        }
    }

    private lateinit var adapter: ComicsAdapter

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        configureView()
        val list = Parcels.unwrap<List<Comic>>(arguments?.getParcelable(KEY_COMICS))
        presenter.prepareView(list)
    }

    override fun onRefresh() {
        presenter.onRefresh()
    }

    override fun onRefreshItems(items: List<Comic>) {
        presenter.onRefreshItems(items)
    }

    override fun itemsUpdated(items: List<Comic>) {
        adapter.update(items)
        comics_swipe.isRefreshing = false
    }

    override fun showNoItemsVisibility(visibility:Boolean) {
        comics_empty.visibility = if (visibility)View.VISIBLE else View.GONE
    }


    override fun refreshItems(itemCount: Int) {
        callback.refreshItems(itemCount)
    }

    override fun onComicSelected(comic: Comic) {
        callback.onComicSelected(comic)
    }

    private fun configureView() {
        comics_swipe.setOnRefreshListener(this)

        val llm = LinearLayoutManager(context)
        comics_rv.layoutManager = llm
        comics_rv.setHasFixedSize(true)
        adapter = ComicsAdapter(this)
        comics_rv.adapter = adapter
        comics_rv.addOnScrollListener(object : EndlessRecyclerViewScrollListener(llm) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.onRefresh(adapter.itemCount)
            }

        })
    }

    override fun getFragmentLayout() = R.layout.fragment_comic_list

    override fun inject(application: ApplicationProject) {
        DaggerListComicFragment_ListComicFragmentComponent.builder()
                .mainComponent(application.component)
                .build()
                .inject(this)
    }

    interface Callback : BaseFragment.Callback {
        fun refreshItems(offset: Int)
        fun onComicSelected(comic: Comic)
    }

    @FragmentScope
    @Component(dependencies = arrayOf(MainComponent::class), modules = arrayOf(ListComicFragmentModule::class))
    internal interface ListComicFragmentComponent {

        fun inject(fragment: ListComicFragment)

    }

    @Module
    @ActivityScope
    internal class ListComicFragmentModule {

        @Provides
        fun provideListFragmentPresenter(): ListComicFragmentContract.ListComicFragmentPresenter = ListComicFragmentPresenterImpl()

    }

}

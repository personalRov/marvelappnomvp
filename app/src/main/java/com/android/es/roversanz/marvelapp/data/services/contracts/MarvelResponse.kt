package com.android.es.roversanz.marvelapp.data.services.contracts

data class MarvelResponse<out E : MarvelContractEntity>(
        val status: String?,
        val attributionText: String?,
        val data: MarvelData<E>?)

data class MarvelData<out E : MarvelContractEntity>(
        val offset: Int?,
        val count: Int?,
        val results: List<E>?)

data class MarvelThumbnail(
        val path: String?,
        val extension: String?)

data class ItemResponseList(
        val available: Int?,
        val returned: Int?,
        val collectionURI: String?,
        val items: List<ItemResponse>?)

data class ItemResponse(
        val name: String?,
        val type: String?=null,
        val role: String? = null,
        val resourceURI: String?)

data class TextObject(
        val type: String?,
        val language: String?,
        val text: String?)

data class UrlItemResponse(
        val type: String?,
        val url: String?)

data class PriceItemResponse(
        val type: String?,
        val price: Double?)

data class DateItemResponse(
        val type: String?,
        val date: String?)

interface MarvelContractEntity

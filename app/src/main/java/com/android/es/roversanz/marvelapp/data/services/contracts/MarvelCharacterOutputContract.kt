package com.android.es.roversanz.marvelapp.data.services.contracts

import java.util.*

data class MarvelCharacterOutputContract(
        val id: Int?,
        val name: String?,
        val description: String?,
        val resourceURI: String?,
        val modified: Date?,
        val thumbnail: MarvelThumbnail?,
        val comics: ItemResponseList?,
        val series: ItemResponseList?,
        val stories: ItemResponseList?,
        val events: ItemResponseList?
) : MarvelContractEntity


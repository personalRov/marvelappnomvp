package com.android.es.roversanz.marvelapp.utils.connectivity

import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.text.format.Formatter
import java.net.NetworkInterface

fun Context.isOnline(): Boolean {
    val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val networkInfo = connMgr.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

fun Context.isWifiConnected(): Boolean {
    val wifi = getSystemService(Context.WIFI_SERVICE) as WifiManager
    return wifi.isWifiEnabled
}

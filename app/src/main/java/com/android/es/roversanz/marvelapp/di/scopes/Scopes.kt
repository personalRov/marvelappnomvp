package com.android.es.roversanz.marvelapp.di.scopes

import javax.inject.Scope

@Scope
annotation class ActivityScope

@Scope
annotation class FragmentScope

package com.android.es.roversanz.marvelapp.data.services.transformers

import com.android.es.roversanz.marvelapp.data.model.DateItem
import com.android.es.roversanz.marvelapp.data.model.PriceItem
import com.android.es.roversanz.marvelapp.data.model.SummaryItem
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.data.model.TextItem
import com.android.es.roversanz.marvelapp.data.model.UrlItem
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelComicOutputContract
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelResponse

class ComicTransformer : Transformer<MarvelComicOutputContract, Comic> {


    companion object {
        private val DOT = "."
        private val IMAGE_MEDIUM_SIZE = "/standard_medium"
        private val IMAGE_BIG_SIZE = "/landscape_xlarge"
    }


    override fun transformer(outputContract: MarvelResponse<MarvelComicOutputContract>): List<Comic> = outputContract.data?.results?.map { c ->
        Comic(
                id = c.id ?: 0,
                title = c.title ?: "",
                description = c.description ?: "",
                modifiedDate = c.modified,
                digitalId = c.digitalId ?: 0,
                issueNumber = c.issueNumber ?: 0,
                variantDescription = c.variantDescription ?: "",
                isbn = c.isbn ?: "",
                upc = c.upc ?: "",
                diamondCode = c.diamondCode ?: "",
                ean = c.ean ?: "",
                issn = c.issn ?: "",
                format = c.format ?: "",
                pageCount = c.pageCount ?: 0,
                resourceURI = c.resourceURI ?: "",
                textObjects = c.textObjects?.map { item -> TextItem(type = item.type ?: "", language = item.language ?: "", text = item.text ?: "") } ?: emptyList(),
                urls = c.urls?.map { item -> UrlItem(type = item.type ?: "", url = item.url ?: "") } ?: emptyList(),
                prices = c.prices?.map { item -> PriceItem(type = item.type ?: "", price = item.price ?: 0.0) } ?: emptyList(),
                dates = c.dates?.map { item -> DateItem(type = item.type ?: "", date = item.date ?: "") } ?: emptyList(),
                thumbnail = c.thumbnail?.let { it.path + IMAGE_MEDIUM_SIZE + DOT + it.extension } ?: "",
                picture = c.thumbnail?.let { it.path + IMAGE_BIG_SIZE + DOT + it.extension } ?: "",
                images = c.images?.map { it.path + IMAGE_MEDIUM_SIZE + DOT + it.extension } ?: emptyList(),
                serie = c.series?.let { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") },
                variants = c.variants?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                        ?: emptyList(),
                collections = c.collections?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                        ?: emptyList(),
                collectedIssues = c.collectedIssues?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                        ?: emptyList(),
                charactersSummary = SummaryList(
                        available = c.characters?.available ?: 0,
                        returned = c.characters?.returned ?: 0,
                        collectionURI = c.characters?.collectionURI ?: "",
                        items = c.characters?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                ?: emptyList()),
                creatorsSummary = SummaryList(
                        available = c.creators?.available ?: 0,
                        returned = c.creators?.returned ?: 0,
                        collectionURI = c.creators?.collectionURI ?: "",
                        items = c.creators?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                ?: emptyList()),
                eventsSummary = SummaryList(
                        available = c.events?.available ?: 0,
                        returned = c.events?.returned ?: 0,
                        collectionURI = c.events?.collectionURI ?: "",
                        items = c.events?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                ?: emptyList()),
                storiesSummary = SummaryList(
                        available = c.stories?.available ?: 0,
                        returned = c.stories?.returned ?: 0,
                        collectionURI = c.stories?.collectionURI ?: "",
                        items = c.stories?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                ?: emptyList())
             )
    } ?: emptyList()
}


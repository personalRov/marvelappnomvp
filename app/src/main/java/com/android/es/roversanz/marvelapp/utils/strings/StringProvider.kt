package com.android.es.roversanz.marvelapp.utils.strings

import android.content.Context

interface StringProvider {

    fun getString(resource: Int): String

    fun getString(
            stringResId: Int,
            vararg formatArgs: Any): String

    fun getStringArray(resource: Int): Array<String>

}

class StringProviderImpl(private val context: Context) : StringProvider {

    override fun getString(resource: Int) = context.getString(resource)

    override fun getString(resource: Int, vararg formatArgs: Any) = context.getString(resource, *formatArgs)

    override fun getStringArray(resource: Int) = context.resources.getStringArray(resource)

}

package com.android.es.roversanz.marvelapp.ui.detail

import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.ui.base.BasePresenterImpl
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import javax.inject.Inject

class DetailComicFragmentPresenterImpl @Inject constructor(val stringProvider: StringProvider) :
        BasePresenterImpl<DetailComicFragmentContract.DetailComicFragmentView>(),
        DetailComicFragmentContract.DetailComicFragmentPresenter {

    override fun prepareView(comic: Comic) {
        view?.setComicTitle(comic.title)
        view?.setComicDescription(comic.description)
        view?.loadComicImage(comic.picture)
        view?.setComicCreators(stringProvider.getString(R.string.creators_count, comic.creatorsSummary.available))
        view?.setComicStories(stringProvider.getString(R.string.stories_count, comic.storiesSummary.available))
        view?.setComicEvents(stringProvider.getString(R.string.events_count, comic.eventsSummary.available))
        view?.setComicCharacters(stringProvider.getString(R.string.characters_count, comic.charactersSummary.available))
    }

}

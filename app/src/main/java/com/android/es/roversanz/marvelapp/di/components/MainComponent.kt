package com.android.es.roversanz.marvelapp.di.components

import android.content.Context
import com.android.es.roversanz.marvelapp.data.services.MarvelRepository
import com.android.es.roversanz.marvelapp.data.services.RestMarvelApi
import com.android.es.roversanz.marvelapp.di.modules.ApiModule
import com.android.es.roversanz.marvelapp.di.modules.ApplicationModule
import com.android.es.roversanz.marvelapp.utils.connectivity.ConnectivityProvider
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, ApiModule::class))
interface MainComponent {

    fun provideContext(): Context

    fun provideConnectivityProvider(): ConnectivityProvider

    fun provideStringProvider(): StringProvider

    fun provideRestMarvelApi(): RestMarvelApi

    fun provideMarvelRepository(): MarvelRepository

}

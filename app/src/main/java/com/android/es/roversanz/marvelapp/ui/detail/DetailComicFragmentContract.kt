package com.android.es.roversanz.marvelapp.ui.detail

import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.ui.base.BaseContract

interface DetailComicFragmentContract {

    interface DetailComicFragmentPresenter : BaseContract.BasePresenter<DetailComicFragmentView> {
        fun prepareView(comic: Comic)
    }

    interface DetailComicFragmentView : BaseContract.BaseViewFragment {
        fun setComicTitle(message: String)
        fun setComicDescription(message: String)
        fun setComicDate(message: String)
        fun loadComicImage(image: String)
        fun setComicCreators(message: String)
        fun setComicStories(message: String)
        fun setComicEvents(message: String)
        fun setComicCharacters(message: String)
    }
}

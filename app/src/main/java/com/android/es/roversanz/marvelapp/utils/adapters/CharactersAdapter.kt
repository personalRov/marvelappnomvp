package com.android.es.roversanz.marvelapp.utils.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_character_list.view.character_description
import kotlinx.android.synthetic.main.item_character_list.view.character_image
import kotlinx.android.synthetic.main.item_character_list.view.character_name

class CharactersAdapter(private val listener: CharactersAdapterListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<Character> = mutableListOf()

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): CharacterViewHolder {

        return CharacterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_character_list, parent, false), listener)
    }

    override fun onBindViewHolder(
            holder: RecyclerView.ViewHolder?,
            position: Int) {

        (holder as? CharacterViewHolder)?.onBind(items[position])
    }

    override fun getItemCount() = items.size

    fun add(list: List<Character>) {
        items.addAll(list)
        this.notifyDataSetChanged()
    }

    fun update(list: List<Character>) {
        items.clear()
        add(list)
    }

    class CharacterViewHolder(
            itemView: View,
            listener: CharactersAdapterListener) : RecyclerView.ViewHolder(itemView) {

        private lateinit var character: Character

        init {
            this.itemView?.setOnClickListener {
                listener.onCharacterSelected(character)
            }
        }

        fun onBind(character: Character) {
            this.character = character
            itemView.character_name.text = character.name
            itemView.character_description.text = character.description
            Glide.with(itemView.context)
                    .load(character.thumbnail)
                    .into(itemView.character_image)
        }
    }

}

interface CharactersAdapterListener {

    fun onCharacterSelected(character: Character)

}

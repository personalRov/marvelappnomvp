package com.android.es.roversanz.marvelapp.ui.list

import android.support.annotation.VisibleForTesting
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.ui.base.BasePresenterImpl
import javax.inject.Inject

class ListComicFragmentPresenterImpl @Inject constructor() :
        BasePresenterImpl<ListComicFragmentContract.ListComicFragmentView>(),
        ListComicFragmentContract.ListComicFragmentPresenter {

    @VisibleForTesting
    var items: MutableList<Comic> = mutableListOf()

    override fun prepareView(list: List<Comic>) {
        this.items.clear()
        this.items.addAll(list)
        view?.showNoItemsVisibility(items.isEmpty())
        view?.itemsUpdated(items)
    }

    override fun onRefresh() {
        this.items.clear()
        view?.refreshItems(0)
    }

    override fun onRefresh(itemCount: Int) {
        view?.refreshItems(itemCount)
    }

    override fun onRefreshItems(list: List<Comic>) {
        this.items.addAll(list)
        view?.showNoItemsVisibility(items.isEmpty())
        view?.itemsUpdated(items)
    }

}

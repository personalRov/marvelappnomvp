package com.android.es.roversanz.marvelapp.ui.detail

import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.ui.base.BaseContract

interface DetailCharacterFragmentContract {

    interface DetailCharacterFragmentPresenter : BaseContract.BasePresenter<DetailCharacterFragmentView> {
        fun prepareView(character: Character)
    }

    interface DetailCharacterFragmentView : BaseContract.BaseViewFragment {
        fun setCharacterName(message: String)
        fun setCharacterDescription(message: String)
        fun setCharacterDate(message: String)
        fun loadCharacterImage(image: String)
        fun setCharacterComics(message: String)
        fun setCharacterStories(message: String)
        fun setCharacterEvents(message: String)
        fun setCharacterSeries(message: String)
    }
}

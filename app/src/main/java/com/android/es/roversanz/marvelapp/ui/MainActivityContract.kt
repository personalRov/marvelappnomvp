package com.android.es.roversanz.marvelapp.ui

import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.ui.base.BaseContract

interface MainActivityContract {

    interface MainActivityPresenter : BaseContract.BasePresenter<MainActivityView> {
        fun prepareView()
        fun refreshItems(offset: Int)

        fun onCharacterSelected(character: Character)

        fun onComicSelected(comic: Comic)
    }

    interface MainActivityView : BaseContract.BaseViewActivity {
        fun showCharactersListItems(items: List<Character>)
        fun showCharacterDetail(character: Character)
        fun onRefreshCharactersItems(items: List<Character>)

        fun showComicsListItems(list: List<Comic>)
        fun showComicDetail(comic: Comic)
        fun onRefreshComicsItems(list: List<Comic>)
    }
}

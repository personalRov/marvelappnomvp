package com.android.es.roversanz.marvelapp.ui.base

import android.support.annotation.VisibleForTesting

abstract class BasePresenterImpl<T : BaseContract.BaseView> : BaseContract.BasePresenter<T> {

    @VisibleForTesting
    var view: T? = null

    override fun attach(view: T) {
        this.view = view
    }

    override fun detach() {
        this.view = null
    }

    override fun isAttached() = view != null

}

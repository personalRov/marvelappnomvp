package com.android.es.roversanz.marvelapp.data.services

import android.util.Log
import com.android.es.roversanz.marvelapp.BuildConfig
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.data.services.transformers.CharacterTransformer
import com.android.es.roversanz.marvelapp.data.services.transformers.ComicTransformer
import com.android.es.roversanz.marvelapp.utils.SecurityUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.Date
import javax.inject.Inject

class MarvelRepositoryImpl @Inject constructor(private val marvelApi: RestMarvelApi) : MarvelRepository {

    companion object {
        val TAG = "MarvelRepositoryImpl"
    }

    override fun getCharacters(
            listener: MarvelRepositoryListener<Character>?,
            offset: Int) {

        val apiId = BuildConfig.MARVEL_API_KEY
        val apiSecret = BuildConfig.MARVEL_API_SECRET
        val date = (Date().time).toString()
        val hash = SecurityUtils.md5("$date$apiSecret$apiId")
        val transformer = CharacterTransformer()

        marvelApi.getCharacters(apiId, date, hash, "name", offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ?.map { transformer.transformer(it) }
                ?.subscribe(
                        { list -> listener?.onSuccess(list) },
                        { e ->
                            Log.e(TAG, "ERROR: ${e.message}")
                            listener?.onError(e.message)
                        })
    }

    override fun getComics(
            listener: MarvelRepositoryListener<Comic>?,
            offset: Int) {

        val apiId = BuildConfig.MARVEL_API_KEY
        val apiSecret = BuildConfig.MARVEL_API_SECRET
        val date = (Date().time).toString()
        val hash = SecurityUtils.md5("$date$apiSecret$apiId")
        val transformer = ComicTransformer()
        marvelApi.getComics(apiId, date, hash, "title", offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ?.map { transformer.transformer(it) }
                ?.subscribe(
                        { list -> listener?.onSuccess(list) },
                        { e ->
                            Log.e(TAG, "ERROR: ${e.message}")
                            listener?.onError(e.message)
                        })
    }
}

interface MarvelRepository {
    fun getCharacters(
            listener: MarvelRepositoryListener<Character>?,
            offset: Int)

    fun getComics(
            listener: MarvelRepositoryListener<Comic>?,
            offset: Int)
}

interface MarvelRepositoryListener<E> {
    fun onSuccess(list: List<E>)
    fun onError(message: String?)
}

interface CharacterListener : MarvelRepositoryListener<Character>
interface ComicListener : MarvelRepositoryListener<Comic>

package com.android.es.roversanz.marvelapp.ui.detail

import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.ui.base.BasePresenterImpl
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import java.text.SimpleDateFormat
import javax.inject.Inject

class DetailCharacterFragmentPresenterImpl @Inject constructor(val stringProvider: StringProvider) :
        BasePresenterImpl<DetailCharacterFragmentContract.DetailCharacterFragmentView>(),
        DetailCharacterFragmentContract.DetailCharacterFragmentPresenter {

    companion object {
        private val DATE_PATTERN = "dd/MM/yyyy"
    }

    override fun prepareView(character: Character) {
        view?.setCharacterName(character.name)
        view?.setCharacterDescription(character.description)
        view?.setCharacterDate(SimpleDateFormat(DATE_PATTERN).format(character.modifiedDate))
        view?.loadCharacterImage(character.picture)
        view?.setCharacterComics(stringProvider.getString(R.string.comics_count, character.comicsSummary.available))
        view?.setCharacterStories(stringProvider.getString(R.string.stories_count, character.storiesSummary.available))
        view?.setCharacterEvents(stringProvider.getString(R.string.events_count, character.eventsSummary.available))
        view?.setCharacterSeries(stringProvider.getString(R.string.series_count, character.seriesSummary.available))
    }

}

package com.android.es.roversanz.marvelapp.di.modules

import com.android.es.roversanz.marvelapp.data.services.MarvelRepository
import com.android.es.roversanz.marvelapp.data.services.MarvelRepositoryImpl
import com.android.es.roversanz.marvelapp.data.services.RestMarvelApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {

    private val URL_API_MARVEL = "http://gateway.marvel.com/"

    @Provides
    @Singleton
    fun provideMarvelApi(): RestMarvelApi {
        val builder = getRetrofitBuilder()
        val retrofit = builder.baseUrl(URL_API_MARVEL).build()
        return retrofit.create(RestMarvelApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideMarvelRepository(restMarvelApi: RestMarvelApi): MarvelRepository = MarvelRepositoryImpl(restMarvelApi)

    private fun getRetrofitBuilder(): Retrofit.Builder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getOkHttpClient())

    private fun getOkHttpClient() = OkHttpClient.Builder().build()

}

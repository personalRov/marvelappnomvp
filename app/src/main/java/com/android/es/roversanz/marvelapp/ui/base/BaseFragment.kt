package com.android.es.roversanz.marvelapp.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.es.roversanz.marvelapp.ApplicationProject
import com.android.es.roversanz.marvelapp.app
import javax.inject.Inject

abstract class BaseFragment<in V : BaseContract.BaseView, P : BaseContract.BasePresenter<V>, C : BaseFragment.Callback> : Fragment() {


    @Inject protected lateinit var presenter: P
    protected lateinit var callback: C

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(getFragmentLayout(), container, false)
        callback = activity as C
        activity?.app()?.let { inject(it) }
        presenter.attach(this as V)
        return rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detach()
    }

    abstract fun getFragmentLayout():Int

    abstract fun inject(it: ApplicationProject)

    interface Callback

}

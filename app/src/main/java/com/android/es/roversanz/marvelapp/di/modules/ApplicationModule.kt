package com.android.es.roversanz.marvelapp.di.modules


import android.content.Context
import com.android.es.roversanz.marvelapp.data.services.MarvelRepository
import com.android.es.roversanz.marvelapp.data.services.MarvelRepositoryImpl
import com.android.es.roversanz.marvelapp.utils.connectivity.ConnectivityProvider
import com.android.es.roversanz.marvelapp.utils.connectivity.ConnectivityProviderImpl
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import com.android.es.roversanz.marvelapp.utils.strings.StringProviderImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val ctx: Context) {

    @Provides
    @Singleton
    internal fun provideContext() = ctx

    @Provides
    @Singleton
    internal fun provideStringProvider(ctx: Context): StringProvider = StringProviderImpl(ctx)

    @Provides
    @Singleton
    internal fun provideConnectivityProvider(ctx: Context): ConnectivityProvider = ConnectivityProviderImpl(ctx)

}

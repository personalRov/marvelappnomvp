package com.android.es.roversanz.marvelapp.data.services

import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelCharacterOutputContract
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelComicOutputContract
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RestMarvelApi {

    @GET("/v1/public/characters")
    fun getCharacters(
            @Query("apikey") apiKey: String,
            @Query("ts") time: String,
            @Query("hash") hash: String,
            @Query("orderBy") name: String,
            @Query("offset") offset: Int): Single<MarvelResponse<MarvelCharacterOutputContract>>

    @GET("/v1/public/comics")
    fun getComics(
            @Query("apikey") apiKey: String,
            @Query("ts") time: String,
            @Query("hash") hash: String,
            @Query("orderBy") name: String,
            @Query("offset") offset: Int): Single<MarvelResponse<MarvelComicOutputContract>>
}

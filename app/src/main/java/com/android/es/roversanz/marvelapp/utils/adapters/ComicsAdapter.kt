package com.android.es.roversanz.marvelapp.utils.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_comic_list.view.comic_description
import kotlinx.android.synthetic.main.item_comic_list.view.comic_image
import kotlinx.android.synthetic.main.item_comic_list.view.comic_title

class ComicsAdapter(private val listener: ComicsAdapterListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<Comic> = mutableListOf()

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): ComicViewHolder {

        return ComicViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comic_list, parent, false), listener)
    }

    override fun onBindViewHolder(
            holder: RecyclerView.ViewHolder?,
            position: Int) {

        (holder as? ComicViewHolder)?.onBind(items[position])
    }

    override fun getItemCount() = items.size

    fun add(list: List<Comic>) {
        items.addAll(list)
        this.notifyDataSetChanged()
    }

    fun update(list: List<Comic>) {
        items.clear()
        add(list)
    }

    class ComicViewHolder(
            itemView: View,
            listener: ComicsAdapterListener) : RecyclerView.ViewHolder(itemView) {

        private lateinit var comic: Comic

        init {
            this.itemView?.setOnClickListener {
                listener.onComicSelected(comic)
            }
        }

        fun onBind(comic: Comic) {
            this.comic = comic
            itemView.comic_title.text = comic.title
            itemView.comic_description.text = comic.description
            Glide.with(itemView.context)
                    .load(comic.thumbnail)
                    .into(itemView.comic_image)
        }
    }

}

interface ComicsAdapterListener {

    fun onComicSelected(comic: Comic)

}
